<?php

namespace bookworld\Http\Controllers;

use bookworld\Libro;
use bookworld\Carro;
use Illuminate\Http\Request;
use Srmklive\PayPal\Services\ExpressCheckout;

class LibroController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $libros = Libro::all();
          //la variable de libros es un array
          //y la mandamos a la vista 'index'
      return view('libros.index', compact('libros'));
    }

    //esta función es para poder listar el catalogo en la vista de tienda
    public function tiendaIndex(Request $request)
    {
      //dd($request->get('titulo'));
      $libros = Libro::search($request->get('titulo'))->get();
          //la variable de libros es un array
          //y la mandamos a la vista 'index'


      return view('tienda.index', compact('libros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('libros.libros');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      if ($request->hasFile('imagenLibro')) {
        $file = $request->file('imagenLibro');
        $name = time().$file->getClientOriginalName();
        $file->move(public_path().'/images/',$name);
      }

      $libro = new Libro();
      $libro->titulo = $request->input('titulo');
      $libro->autor = $request->input('autor');
      $libro->editorial = $request->input('editorial');
      $libro->genero = $request->input('genero');
      $libro->cantidad = $request->input('cantidad');
      $libro->precio = $request->input('precio');
      $libro->descripcion = $request->input('descripcion');
      $libro->imagen = $name;
      $libro->save();

      return view('libros.libros');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
          //buscar ese id en la base de datos
        $libro = Libro::find($id);
          //el compact es para compartir infromación con nuestras vistas
        return view('libros.show', compact('libro'));
    }

    //función para mostrar los detalles del libro en la vista de la tienda
    public function tiendaShow($id)
    {
      $libro = Libro::find($id);
      return view('tienda.show', compact('libro'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $libro = Libro::find($id);

        return view('libros.edit', compact('libro'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $libro = Libro::find($id);
        $libro->fill($request->except('imagen'));


        if ($request->hasFile('imagenLibro')) {
          $file = $request->file('imagenLibro');
          $name = time().$file->getClientOriginalName();
          $libro->imagen = $name;
          $file->move(public_path().'/images/',$name);
        }

        $libro->save();

        return view('libros.show', compact('libro'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $libro = Libro::find($id);
        $libro->delete();

        return view('libros.eliminado');
    }

    public function vaciarCarrito()
    {
        $libro = Carro::all();

        foreach ($libro as $li) {

          $li->delete();
        }

    }


    /**
    *Función para ver el la vista que contine al carrito
    **/

    public function tiendaCarrito()
    {
      return view('tienda.carrito');
    }



    /**
    *Función para agregar un producto al carrito
    **/

    public function agregarCarrito($id)
    {
        $libro = Libro::find($id);

        return view('tienda.carrito', compact('libro'));
    }


    public function paypalSuccess(Request $request){
      $provider = new ExpressCheckout;

      $token = $request->token;
      $PayerID = $request->PayerID;

      $response = $provider->getExpressCheckoutDetails($token);

      $data = $this->cartData();

      $response = $provider->doExpressCheckoutPayment($data, $token, $PayerID);

      $this->vaciarCarrito();

      return redirect('/tienda')->with('status','Pago realizado con exito');

    }

    public function payWithPaypal(){
      $provider = new ExpressCheckout;

      $data = $this->cartData();

      $response = $provider->setExpressCheckout($data);


      //dd($response);
      // This will redirect user to PayPal
      return redirect($response['paypal_link']);
    }

    protected function cartData(){
      $libros = Carro::all();
      
      $data = [];
      
      $data['items'] =[];
      foreach ($libros as $libro) {
      $itemDetail = [
              'name' => $libro->libro,
              'price' => $libro->precio,
              'qty' => $libro->cantidad
      ];
      $data['items'][] = $itemDetail;
    }


      $data['invoice_id'] = uniqid();
      $data['invoice_description'] = "Order #{$data['invoice_id']} Invoice";
      $data['return_url'] = url('/payment/success/');
      $data['cancel_url'] = url('/cart');

      $total = 0;
      foreach($data['items'] as $item) {
          $total += $item['price']*$item['qty'];
      }

      $data['total'] = $total;

      return $data;
    }
}
