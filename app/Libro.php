<?php

namespace bookworld;

use Illuminate\Database\Eloquent\Model;

class Libro extends Model
{
    protected $fillable = ['titulo','autor','editorial','genero','cantidad','precio','descripcion','imagen'];

    public function scopeSearch($query,$titulo)
    {

      if ($titulo != "") {
          return $query->where('titulo','LIKE','%'.$titulo.'%');
      }

    }
}
