<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/carrito', function () {
    return view('carro');
});
Route::get('/carrito2', function () {
    return view('carrito');
});

Route::get('/elemento', function () {
    return view('elemento');
});
Route::get('/ajax', function () {
    return view('ajax');
});

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/name/{name?}', function ($name = null){
  return 'Hola soy '.$name;
});


Route::get('prueba', 'PruebaController@prueba');


*/

Route::resource('libros', 'LibroController');
//Route::resource('/', 'LibroController');

Route::resource('ventas','ventasController');


Route::get('tienda', 'LibroController@tiendaIndex');
Route::get('tienda/{id}','LibroController@tiendaShow');
Route::get('carrito','LibroController@tiendaCarrito');
Route::get('carrito/add/{id}','LibroController@agregarCarrito');

Route::get('carrito/paymetPaypal/','LibroController@payWithPaypal');
Route::get('payment/success','LibroController@paypalSuccess');


Route::get('/datosdecompra', function () {
    return view('ventas.datos');
});

Route::resource('carrito2','CarroController');
