@extends('layouts.app3')

@section('title', 'Ventas')

@section('content')

  <blockquote class="blockquote text-center">
    <br>
  <p class="mb-0">Ventas realizadas</p>
  <footer class="blockquote-footer"><cite title="Source Title">Bookworld company la mejor tienda para los mejores libros</cite></footer>
</blockquote>

<div class="container">
  <table class="table table-borderless" style="margin:20px; margin-top:80px;">
        <thead class="">
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Libro</th>
            <th scope="col">Cantidad</th>
            <th scope="col">total</th>
            <th scope="col">Cliente</th>
            <th scope="col">Fecha</th>
          </tr>
        </thead>
          <tbody>
            @foreach($ventas as $venta)
            <tr>
              <td>{{$venta->id}}</td>
              <td>{{$venta->producto}}</td>
              <td>{{$venta->cantidad}}</td>
              <td>{{$venta->total}}</td>
              <td>{{$venta->cliente}}</td>
              <td>{{$venta->created_at}}</td>
            </tr>
           @endforeach
          </tbody>
  </table>
</div>

<div>
  <button class="btn btn-success btn-lg rounded float-right" type="submit" style="margin-right: 50px;" onclick="detalles()" >Ver más detalles en Paypal</button>
</div>



@endsection

<script type="text/javascript">


function detalles(){
    window.open('https://www.sandbox.paypal.com/us/signin','_blank');
  }

</script>
