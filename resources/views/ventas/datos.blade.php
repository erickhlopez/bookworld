@extends('layouts.app2')

@section('title', 'Datos-cliente')
@section('content')
  <h1 class="container">Ingresa tus datos</h1>
<form class="form-group" action="/ventas" method="post" enctype="multipart/form-data">
@csrf
<div class="input-group input-group-lg">
  <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-lg">Nombre completo</span>
  </div>
  <input type="text" name="nombre" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm" required>
</div><br>

<div class="input-group input-group-lg">
  <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-lg">Correo electrónico</span>
  </div>
  <input type="text" name="correo" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm" required>
</div><br>

<div class="input-group input-group-lg">
  <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-lg">Telefono</span>
  </div>
  <input type="text" name="telefono" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm" required>
</div><br>

<div class="input-group input-group-lg">
  <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-lg">Sugerencias (opcional)</span>
  </div>
  <input type="text" name="descripcion" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"  maxlength="185">
</div><br>


<div>
  <button type="submit" class="btn btn-primary btn-lg btn-block"  onclick="ajax()">Pagar con PayPal</button>
  <a class="btn btn-danger btn-lg btn-block" href="/carrito2" >Cancelar</a>
</div>


</form>

<script type="text/javascript">

function ajax(){
$.get('/users.php', { userId : 1234 }, function(resp) {
    console.log(resp);
});
}

</script>

@endsection
