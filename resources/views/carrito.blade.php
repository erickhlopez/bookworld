@extends('layouts.app')

@section('title', 'Libros')
@section('content')

<div class="container">
	<table class="table table-striped table-dark" style="margin:20px">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">nombre</th>
      <th scope="col">Desc</th>
      <th scope="col">Precio unitario</th>
      <th scope="col">cant</th>
      <th scope="col">total</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Zapatos</td>
      <td>Zapatos para hombre cafes</td>
      <td>	<div id="precio1">100</div></td>
      <td><button class="btn btn-outline-info btn-sm" onclick="bajarCantidad('1')">-</button> <cn id="cant1">1</cn> <button class="btn btn-outline-info btn-sm" onclick="subirCantidad('1')">+</button></td>
      <td><div id="total1" >0</div></td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Camisas</td>
      <td>Camisas para hombres cafe</td>
      <td>	<div id="precio2">250</div></td>
      <td><button class="btn btn-outline-info btn-sm" onclick="bajarCantidad('2')">-</button> <cn id="cant2">1</cn> <button class="btn btn-outline-info btn-sm" onclick="subirCantidad('2')">+</button></td>
      <td><div id="total2" >0</div></td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Playeras</td>
      <td>Playera cafe para dama</td>
      <td>	<div id="precio3">380</div></td>
      <td><button class="btn btn-outline-info btn-sm" onclick="bajarCantidad('3')">-</button> <cn id="cant3">1</cn> <button class="btn btn-outline-info btn-sm" onclick="subirCantidad('3')">+</button></td>
      <td><div id="total3" >0</div></td>
    </tr>
  </tbody>
</table>
</div>
<script type="text/javascript">
	function bajarCantidad(id){
		var can = document.getElementById("cant"+id);
		var cann = can.innerHTML;
		var integerCan = parseInt(cann, 10);
		var nuevo = integerCan-1;
		console.log(nuevo);
		can.innerHTML = nuevo;
		total(id);
	}
	function subirCantidad(id){
		var can = document.getElementById("cant"+id) ;
		var cann = can.innerHTML;
		var integerCan = parseInt(cann, 10);
		var cann = can.innerHTML;
		var nuevo = integerCan+1;
		console.log(nuevo);
		can.innerHTML = nuevo;
		total(id);
	}
	function total(id){
		var can = document.getElementById("cant"+id).innerHTML ;
		var preU = document.getElementById("precio"+id).innerHTML ;
		var integerCan = parseInt(can, 10);
		var integerPre = parseInt(preU, 10);
		var total = integerPre * integerCan;
		console.log(total) ;
		var totalh = document.getElementById("total"+id);
		totalh.innerHTML = total;
	}
	window.onload=total(1);
	window.onload=total(2);
	window.onload=total(3);

</script>


@endsection
