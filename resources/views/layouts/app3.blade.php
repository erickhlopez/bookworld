<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Bookworld - @yield('title')</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  </head>
  <body >

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav">
        <a class="nav-item nav-link" href="/libros">Catalogo</a>
        <a class="nav-item nav-link" href="/libros/create">Nuevo Libro</a>
        <a class="nav-item nav-link" href="/ventas">Ventas</a>
        <a class="nav-item nav-link" href="/tienda">Ir a la tienda</a>
      </div>
   </div>
</nav>

    <div style="margin-bottom: 100px;" class="container">

        @yield('content')

    </div>

  </body>
</html>
