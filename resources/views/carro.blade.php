@extends('layouts.app')

@section('title', 'Libros')
@section('content')

  <div class="container">
	<table class="table table-striped table-dark" style="margin: 20px" id="tabla">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Producto</th>
      <th scope="col">Descripcion</th>
      <th scope="col">Precio unitario</th>
      <th scope="col">Cantidad</th>
      <th scope="col">Total</th>
    </tr>
  </thead>
  <tbody id="tbody">
    <tr>
      <td>Gorra</td>
      <td>Gorra color azul</td>
      <td>350</td>
      <td><button class="btn btn-outline-info btn-sm" value="-1">-</button> <span>0</span> <button class="btn btn-outline-info btn-sm" value="1">+</button></td>
      <td>0</td>
    </tr>
    <tr>
      <td>Pantalon</td>
      <td>Pantalon de mezclilla</td>
      <td>400</td>
      <td><button class="btn btn-outline-info btn-sm" value="-1">-</button> <span>0</span> <button class="btn btn-outline-info btn-sm" value="1">+</button></td>
      <td>0</td>
    </tr>
    <tr>
      <td>Playera</td>
      <td>Playera color verde</td>
      <td>120</td>
      <td><button class="btn btn-outline-info btn-sm" value="-1">-</button> <span>0</span> <button class="btn btn-outline-info btn-sm" value="1">+</button></td>
      <td>0</td>
    </tr>
    <tr>
      <td>Lentes</td>
      <td>Lentes de sol</td>
      <td>340</td>
      <td><button class="btn btn-outline-info btn-sm" value="-1">-</button> <span>0</span> <button class="btn btn-outline-info btn-sm" value="1">+</button></td>
      <td>0</td>
    </tr>
  </tbody>
</table>

<button class="btn btn-outline-info"  onclick="agregarFila()" style="gravity:cenert">Nuevo</button>
</div>

<script type="text/javascript">
  sumar();

function agregarFila(){
  document.getElementById("tabla").insertRow(-1).innerHTML = '<td>Playera</td><td>Playera color verde</td><td>120</td><td><button class="btn btn-outline-info btn-sm" value="-1">-</button> <span>0</span> <button class="btn btn-outline-info btn-sm" value="1">+</button></td><td>0</td>';

}
function sumar(){
var botones = document.querySelectorAll('.btn');
console.log(botones);

botones.forEach(function(btn){
  console.log(btn);
  btn.addEventListener('click', function(e){

    var boton = e.target;
    var val = parseInt(boton.value, 10);
    console.log(val);

    var tr = boton.parentNode.parentNode;
    //console.log(tr);

   var tds = tr.querySelectorAll("td");
  //  console.log(tds);

    var vu = parseInt(tds[2].innerHTML, 10);
  //  console.log(vu);

    var cant = parseInt(tds[3].querySelector("span").innerHTML, 10);
    if(val == 1)cant = cant + 1;
    if(val == -1)cant = cant -1;
    tds[3].querySelector("span").innerHTML = cant;

    //console.log(cant);

    var total = vu * cant;
    tds[4].innerHTML = total;
  //  console.log(total);



  })
})
}

</script>

@endsection
