@extends('layouts.app3')

@section('title', 'Libros')

@section('content')

  <img src="/images/{{$libro->imagen}}" class="rounded float-left"  style="width: 350px; height: 400px; margin-top: 60px;">
  <div >
    <p class="font-weight-lighter text-right" style="margin-right: 180px; margin-top: 60px;"> <font size=10 color=#A78A84>  {{$libro->titulo}} </font> </p>
  </div>
  <div >
    <p class="font-weight-lighter text-right" > <font size=6 color=#837A78> Autor: {{$libro->autor}} </font> </p>
  </div>
  <div >
    <p class="font-weight-lighter text-right" > <font size=6 color=#837A78> Editorial: {{$libro->editorial}} </font> </p>
  </div>
  <div >
    <p class="font-weight-lighter text-right" > <font size=6 color=#837A78> Genero: {{$libro->genero}} </font> </p>
  </div>
  <div >
    <p class="font-weight-lighter text-right" > <font size=6 color=#837A78> Precio: ${{$libro->precio}} </font> </p>
  </div>
  <div >
    <p class="font-weight-lighter text-right" > <font size=6 color=#837A78> Existencia: {{$libro->cantidad}} </font> </p>
  </div>
  <div >
    <p class="font-weight-lighter text-center font-italic" style="margin-right: 50px; margin-left: 450px; margin-top: 50px;"> <font size=5 color=#837A78> {{$libro->descripcion}} </font> </p>
  </div>

  <form method="POST" action="/libros/{{$libro->id}}">
        @csrf
        @method('DELETE')
        <div style="margin-top: 100px;">
          <a class="btn btn-dark  btn-lg rounded float-left " href="/libros">Regresar</a>
          <a class="btn btn-warning  btn-lg rounded float-right" href="/libros/{{$libro->id}}/edit">Editar</a>
          <button type="submit" class="btn btn-danger btn-lg rounded float-right" style="margin-right: 50px;">Eliminar</button>
        </div>
    </form>﻿


@endsection
