@extends('layouts.app3')

@section('title', 'Libros')
@section('content')
  <h1 class="container">Agrega un nuevo libro</h1>
<form class="form-group" action="/libros" method="post" enctype="multipart/form-data">
@csrf
<div class="input-group input-group-lg">
  <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-lg">Titulo de Libro</span>
  </div>
  <input type="text" name="titulo" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm" required>
</div><br>

<div class="input-group input-group-lg">
  <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-lg">Autor</span>
  </div>
  <input type="text" name="autor" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm" required>
</div><br>

<div class="input-group input-group-lg">
  <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-lg">Editorial</span>
  </div>
  <input type="text" name="editorial" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm" required>
</div><br>

<div class="input-group input-group-lg">
  <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-lg">Genero</span>
  </div>
  <input type="text" name="genero" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm" required>
</div><br>

<div class="input-group input-group-lg">
  <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-lg"> Agrega la existencia del libro</span>
  </div>
    <input type="number" name="cantidad" value="0" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm" min="0" required>
</div><br>

<div class="input-group input-group-lg">
  <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-lg">Precio</span>
  </div>
  <input type="number" name="precio" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm" min="0" required>
</div><br>

<div class="input-group input-group-lg">
  <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-lg">Descripcion</span>
  </div>
  <input type="text" name="descripcion" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm" required maxlength="185">
</div><br>

<div class="form-group">
  <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-lg">Selecciona la portada del libro</span>
  </div>
    <input type="file" class="form-control-file" id="exampleFormControlFile1" name="imagenLibro">
</div>


<div>
  <button type="submit" class="btn btn-primary btn-lg btn-block" href="/libros" onclick="ajax()">Agregar</button>
  <a class="btn btn-danger btn-lg btn-block" href="/libros" >Cancelar</a>
</div>


</form>

<script type="text/javascript">

function ajax(){
$.get('/users.php', { userId : 1234 }, function(resp) {
    console.log(resp);
});
}

</script>

@endsection
