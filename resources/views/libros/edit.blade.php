@extends('layouts.app3')

@section('title', 'Libros edit')
@section('content')
  <h1 class="container">Editando el libro {{$libro->titulo}}</h1>
<form class="form-group" action="/libros/{{$libro->id}}" method="post" enctype="multipart/form-data">
    @method('PUT')
    @csrf
    <div class="input-group input-group-lg">
      <div class="input-group-prepend">
        <span class="input-group-text" id="inputGroup-sizing-lg">Titulo de Libro</span>
      </div>
      <input type="text" name="titulo" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm" value="{{$libro->titulo}}" required>
    </div><br>

    <div class="input-group input-group-lg">
      <div class="input-group-prepend">
        <span class="input-group-text" id="inputGroup-sizing-lg">Autor</span>
      </div>
      <input type="text" name="autor" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm" value="{{$libro->autor}}" required>
    </div><br>

    <div class="input-group input-group-lg">
      <div class="input-group-prepend">
        <span class="input-group-text" id="inputGroup-sizing-lg">Editorial</span>
      </div>
      <input type="text" name="editorial" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm" value="{{$libro->editorial}}" required>
    </div><br>

    <div class="input-group input-group-lg">
      <div class="input-group-prepend">
        <span class="input-group-text" id="inputGroup-sizing-lg">Genero</span>
      </div>
      <input type="text" name="genero" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm" value="{{$libro->genero}}" required >
    </div><br>

    <div class="input-group input-group-lg">
      <div class="input-group-prepend">
        <span class="input-group-text" id="inputGroup-sizing-lg"> Agrega la existencia del libro</span>
      </div>
        <input type="number" name="cantidad" value="0" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm" min="0" value="{{$libro->cantidad}}" required>
    </div><br>

    <div class="input-group input-group-lg">
      <div class="input-group-prepend">
        <span class="input-group-text" id="inputGroup-sizing-lg">Precio</span>
      </div>
      <input type="number" name="precio" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm" min="0" value="{{$libro->precio}}" required>
    </div><br>

    <div class="input-group input-group-lg">
      <div class="input-group-prepend">
        <span class="input-group-text" id="inputGroup-sizing-lg">Descripcion</span>
      </div>
      <input type="text" name="descripcion" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm" value="{{$libro->descripcion}}" required maxlength="185">
    </div><br>

    <div class="form-group">
      <div class="input-group-prepend">
        <span class="input-group-text" id="inputGroup-sizing-lg">Selecciona la portada del libro</span>
      </div>
        <input type="file" class="form-control-file" id="exampleFormControlFile1" name="imagenLibro">
    </div>


    <div>
      <button type="submit" class="btn btn-primary btn-lg btn-block" href="/libros" onclick="ajax()">Actualizar</button>
      <a class="btn btn-danger btn-lg btn-block" href="/libros" >Cancelar</a>
    </div>


</form>

<script type="text/javascript">

function ajax(){
$.get('/users.php', { userId : 1234 }, function(resp) {
    console.log(resp);
});
}

</script>

@endsection
