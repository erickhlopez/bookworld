<!DOCTYPE html>
<html>
<head>
  <title>||Trabajando con elementos||</title>
</head>
<body>
  <div id="div1"></div>

  <button onclick="addElement()">Agregar Elemento</button>

  <script type="text/javascript">
  function addElement () {
  // crea un nuevo div
  // y añade contenido
  var newDiv = document.createElement("div");
  var newContent = document.createTextNode("Hola!¿Qué tal?");
  newDiv.appendChild(newContent); //añade texto al div creado.

  // añade el elemento creado y su contenido al DOM
  newDiv.classList.add('my-new-class', 'my-other-class');
  newDiv.setAttribute("onclick", "funcio1Creada()");

  var currentDiv = document.getElementById("div1");
  document.body.insertBefore(newDiv, currentDiv);

}

function funcio1Creada(){
  console.log('hola ');
}

  </script>
</body>
</html>
