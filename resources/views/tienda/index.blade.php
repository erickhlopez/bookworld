@extends('layouts.app2')

@section('title', 'Libros')

@section('content')
@if(session('status'))
    <div class="alert alert-success">
      {{session('status')}}
    </div>
@endif

  <blockquote class="blockquote text-center">
    <br>
  <p class="mb-0">Libros en venta</p>
  <footer class="blockquote-footer"><cite title="Source Title">Bookworld company la mejor tienda para los mejores libros</cite></footer>
</blockquote>
<hr>

<form class="navbar-form navbar-left pull-rigth" role="search" action="tienda" method="get">
  <div class="form-group">
    <input type="text" name="titulo" class="form-control" placeholder="Buscar libro..." >
  </div>
</form>

<div class="row">
  @foreach ($libros as $libro)
  <div class="col-sm">
    <div class="card text-center" style="width: 300px; background-color: #D7DDE0; margin-top: 40px;">
     <img src="images/{{$libro->imagen}}" class="card-img-top mx-auto d-block"  style="width: 150px; height: 190px; margin-top: 20px;">
      <div class="card-body">
        <h5 class="card-title">{{$libro->titulo}}</h5>
        <p class="card-text">Autor: {{$libro->autor}}</p>
        <a class="btn btn-primary" href="/tienda/{{$libro->id}}">Detalles del libro</a>
      </div>
     </div>
    </div>
    @endforeach
  </div>

@endsection
