@extends('layouts.app2')

@section('title', 'Carrito')

@section('content')
@if(session('status'))
    <div class="alert alert-success">
      {{session('status')}}
    </div>
@endif

        <div class="container">
          <table class="table table-borderless" style="margin:20px; margin-top:80px;">
                <thead class="">
                  <tr>
                    <th scope="col">Nombre</th>
                    <th scope="col">Precio unitario</th>
                    <th scope="col">Cantidad</th>
                    <th scope="col">Total</th>
                  </tr>
                </thead>
                  <tbody>
                    @foreach($carrito as $libros)
                    <tr>
                      <td>{{$libros->libro}}</td>
                      <td>{{$libros->precio}}</td>
                      <td><button class="btn btn-outline-info btn-sm" value="-1">-</button> <span>{{$libros->cantidad}}</span> <button class="btn btn-outline-info btn-sm" value="1">+</button></td>
                      <td>{{$libros->precio}}</td>
                      <form method="POST" action="/carrito2/{{$libros->id}}">
                        @csrf
                        @method('DELETE')
                      <td><button class="btn btn-danger btn-sm rounded">Eliminar</button></td>
                      </form>﻿
                    </tr>
                   @endforeach
                  </tbody>
          </table>

          <a class="btn btn-success btn-lg rounded float-right" style="margin-right: 50px;" href="/datosdecompra">Continuar con la compra...</a>
        </div>




<script type="text/javascript">

sumar();

function sumar(){
var botones = document.querySelectorAll('.btn');
console.log(botones);

botones.forEach(function(btn){
  console.log(btn);
  btn.addEventListener('click', function(e){

    var boton = e.target;
    var val = parseInt(boton.value, 10);
    console.log(val);

    var tr = boton.parentNode.parentNode;
    //console.log(tr);

   var tds = tr.querySelectorAll("td");
  //  console.log(tds);

    var vu = parseInt(tds[1].innerHTML, 10);
  //  console.log(vu);

    var cant = parseInt(tds[2].querySelector("span").innerHTML, 10);
    if(val == 1)cant = cant + 1;
    if(val == -1)cant = cant -1;
    tds[2].querySelector("span").innerHTML = cant;

    //console.log(cant);

    var total = vu * cant;
    tds[3].innerHTML = total.toFixed(2);
  console.log(total);

  })
})
}

</script>



@endsection
