@extends('layouts.app2')

@section('title', 'Detalles')

@section('content')

<form class="form-group" method="POST" action="/carrito2">
  @csrf
<div style="margin-bottom: 200px;">
<img src="/images/{{$libro->imagen}}" class="rounded float-left"  style="width: 350px; height: 400px; margin-top: 60px;">
<div>
  <p class="font-weight-lighter text-right" style="margin-right: 180px; margin-top: 60px;"> <font size=10 color=#A78A84>  {{$libro->titulo}} <input name="libro" value="{{$libro->titulo}}" hidden></font> </p>
</div>
<div >
  <p class="font-weight-lighter text-right" > <font size=6 color=#837A78> Autor: {{$libro->autor}} </font> </p>
</div>
<div >
  <p class="font-weight-lighter text-right" > <font size=6 color=#837A78> Editorial: {{$libro->editorial}} </font> </p>
</div>
<div >
  <p class="font-weight-lighter text-right" > <font size=6 color=#837A78> Genero: {{$libro->genero}} </font> </p>
</div>
<div >
  <p class="font-weight-lighter text-right" > <font size=6 color=#837A78> Precio: ${{$libro->precio}} <input name="precio" value="{{$libro->precio}}" hidden></font> </p>
</div>
<br>
<div >
  <p class="font-weight-lighter text-center font-italic" style="margin-right: 50px; margin-left: 450px; margin-top: 50px;"> <font size=5 color=#837A78> {{$libro->descripcion}} </font> </p>
</div>


      <div style="margin-top: 100px;">
        <a class="btn btn-dark  btn-lg rounded float-left " href="/tienda">Regresar</a>
        <button class="btn btn-success btn-lg rounded float-right" type="submit" style="margin-right: 50px;" >Agregar al carrito</button>
      </div>

      </div>
      </form>


@endsection
